<?php

/* @var $this yii\web\View */
$this->title = 'Карточки!';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Карточки!</h1>
    </div>
    <div class="body-content">
        <div class="row">
            <?php foreach ($cards as $card) { ?>
                <div class="col-md-2 col-xs-12">
                    <div class="col-xs-12">Нименование:<?= $card->name ?></div>
                    <div class="col-xs-12"><img src="/<?= $card->images->source ?>" width="100" height="100" alt="image"/></div>
                    <div class="col-xs-12">Описание:<?= $card->description ?></div>
                    <div class="col-xs-12"><span class="glyphicon glyphicon-eye-open"></span><?= $card->views ?></div>
                    <div class="col-xs-12"><a href="/site/card?id=<?= $card->id ?>" class="btn btn-primary">Смотреть</a>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
</div>
