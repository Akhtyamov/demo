<?php
$this->title = 'Карточка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">Наименование карточки: <?= $card->name ?></div>
    <div class="col-xs-12"><img src="/<?= $card->images->source ?>" width="100" height="100" alt="image"/></div>
    <div class="col-xs-12">Описание:<?= $card->description ?></div>
    <div class="col-xs-12"><span class="glyphicon glyphicon-eye-open"></span><?=$card->views?></div>
</div>
<?php
$card->views += 1;
$card->update(false);
?>