<?php
namespace common\models;

class CardsElastic extends \yii\elasticsearch\ActiveRecord
{
    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {

        return ['_id', 'name', 'description', 'views','image'];
    }

}