<?php

namespace common\models\index;

use \yii\elasticsearch\ActiveRecord;

class ElasticCards extends ActiveRecord
{
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id',
                    'name',
                    'description',
                    'views',
                    'image'
                ]
            ]
        ];
    }

    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    public static function createIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->createIndex(static::index(), [
            'mappings' => static::mapping(),
        ]);
    }

    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }
}