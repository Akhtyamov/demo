<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cards".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 */
class Cards extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description','image'], 'required'],
            [['name'], 'string', 'max' => 200],
            ['views','integer','max'=>4],
            [['description'], 'string', 'max' => 600],
            [['image'], 'file', 'skipOnEmpty' =>false,'maxSize' => 1024 * 2048 * 1,'extensions' => ['jpg', 'png','jpeg']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
        ];
    }

    public function getImages(){

        return $this->hasOne(Images::className(),['owner_id'=>'id']);
    }

    public function uploadImage(){
        if($this->validate()){
            $fullWay = 'upload/'.md5(date('d-m-Y-i-s')).".".$this->image->extension;
            if($this->image->saveAs($fullWay)){
              $img = new Images();
              $img->owner_id = $this->id;
              $img->source = $fullWay;
              $img->save(false);
            }

        }
    }
}
