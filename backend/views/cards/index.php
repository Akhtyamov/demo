<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchCards */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Карточки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cards-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить карточку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'description',
            [
                'label' => 'Картинка',
                'format' => 'raw',
                'value' => function ($data) {
                    return "<img src='/{$data->images->source}' alt='image' width='100' height='100'/>";
                },
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete} '],
        ],
    ]); ?>


</div>
