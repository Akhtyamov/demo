<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cards */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cards-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php if($model->isNewRecord || empty($model->images->source)) {?>
    <?= $form->field($model, 'image')->fileInput()->label(false) ?>
    <?php } else { ?>
        <div class="col-xs-12  text-center">
            <div class="col-xs-12"><img src='/<?= $model->images->source ?>' alt='image' width="100" height="100" class="img"/></div>
            <div class="col-xs-12"><br/><a href="/cards/drop-image?card_id=<?=$model->id?>" class="btn btn-danger">Удалить картинку</a></div>
        </div>
    <?php } ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
