<?php

use yii\db\Schema;
use yii\db\Migration;

class m191129_004340_user extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user}}',
            [
                'id'=> $this->primaryKey(11),
                'username'=> $this->string(100)->notNull(),
                'status'=> $this->integer(11)->notNull(),
                'password'=> $this->string(100)->notNull(),
                'auth_key'=> $this->string(100)->notNull(),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
