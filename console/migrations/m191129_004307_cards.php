<?php

use yii\db\Schema;
use yii\db\Migration;

class m191129_004307_cards extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%cards}}',
            [
                'id'=> $this->primaryKey(3),
                'name'=> $this->string(200)->notNull(),
                'description'=> $this->string(600)->notNull(),
                'views'=> $this->integer(4)->notNull()->defaultValue(0),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%cards}}');
    }
}
