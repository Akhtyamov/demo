<?php

use yii\db\Schema;
use yii\db\Migration;

class m191129_004332_images extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%images}}',
            [
                'id'=> $this->primaryKey(3),
                'owner_id'=> $this->integer(3)->notNull(),
                'source'=> $this->string(50)->notNull(),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%images}}');
    }
}
